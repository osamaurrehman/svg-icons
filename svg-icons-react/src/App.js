import UserSVG from './svg/UserSVG';
import FacebookSVG from './svg/FacebookSVG';
import InstaSVG from './svg/InstaSVG';
import './App.css';

function App() {
  return (
    <div className="container">
      <ul className="app">
        <li>
          <UserSVG className="si-user-icon" />
        </li>
        <li>
          <FacebookSVG className="custom-fb-icon" />
        </li>
        <li>
          <InstaSVG />
        </li>
      </ul>
    </div>
  );
}

export default App;
